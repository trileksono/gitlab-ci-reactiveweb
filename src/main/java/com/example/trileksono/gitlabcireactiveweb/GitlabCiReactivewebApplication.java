package com.example.trileksono.gitlabcireactiveweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EnableR2dbcRepositories(basePackages = "com.example.trileksono.gitlabcireactiveweb.repo")
public class GitlabCiReactivewebApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabCiReactivewebApplication.class, args);
	}

}
