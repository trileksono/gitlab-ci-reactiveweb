package com.example.trileksono.gitlabcireactiveweb.repo;

import com.example.trileksono.gitlabcireactiveweb.model.MStudent;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface StudentRepo extends ReactiveCrudRepository<MStudent, Integer> {
}
