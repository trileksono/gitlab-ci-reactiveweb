package com.example.trileksono.gitlabcireactiveweb.config;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;

@Configuration
public class DatabaseConfig extends AbstractR2dbcConfiguration {

  @Override public ConnectionFactory connectionFactory() {
    return new PostgresqlConnectionFactory(
        PostgresqlConnectionConfiguration.builder()
            .host("ec2-23-23-173-30.compute-1.amazonaws.com")
            .database("d6etluvq3pcrig")
            .username("yjkyunpkhosfaj")
            .password("9d7f1949005447a9d04c2d9932e7991502a0547fe7a1eaaa01bfb25ca8d3466a")
            .build());
  }
}
