package com.example.trileksono.gitlabcireactiveweb.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@Table("student")
public class MStudent {

  @Id
  private Integer id;
  private String name;
  private String address;
}
