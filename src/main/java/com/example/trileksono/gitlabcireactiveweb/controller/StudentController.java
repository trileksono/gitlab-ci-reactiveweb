package com.example.trileksono.gitlabcireactiveweb.controller;

import com.example.trileksono.gitlabcireactiveweb.model.MStudent;
import com.example.trileksono.gitlabcireactiveweb.repo.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/student")
public class StudentController {

  @Autowired
  private StudentRepo mStudentRepo;

  @GetMapping("/{id}")
  private Mono<MStudent> getStudentById(@PathVariable("id") int id) {
    return mStudentRepo.findById(id);
  }

  @GetMapping
  private Flux<MStudent> getAllStudent() {
    return mStudentRepo.findAll();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  private Mono<MStudent> saveStudent(@RequestBody MStudent mStudent) {
    return mStudentRepo.save(mStudent);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  private void deleteStudent(@PathVariable("id") int id) {
    mStudentRepo.deleteById(id);
  }

  @PutMapping("/{id}")
  private Mono<MStudent> updateStudent(@PathVariable("id") int id, @RequestBody MStudent updateStudent) {
    return mStudentRepo.findById(id)
        .map(mStudent -> {
          updateStudent.setId(mStudent.getId());
          return updateStudent;
        })
        .flatMap(updateObject -> mStudentRepo.save(updateObject));
  }
}
