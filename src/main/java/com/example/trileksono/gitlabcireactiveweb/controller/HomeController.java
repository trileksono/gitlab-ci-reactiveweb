package com.example.trileksono.gitlabcireactiveweb.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

  @GetMapping("/")
  public ResponseEntity<String> getHome(){
    return new ResponseEntity<>("Hello from heroku ", HttpStatus.OK);
  }

}
